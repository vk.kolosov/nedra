package vk.intern.nedra.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vk.intern.nedra.model.UserInfo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    void findByUseripOrCreate() {

        //given
        String input = "ip_example";
        String expectedResult = "ip_example";

        //run
        String result = userService.findByUseripOrCreate(input).getUserip();

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    @Test
    void save() {

        //given
        UserInfo input = new UserInfo("ip_example", 5);
        Integer expectedResult = 5;

        //run
        userService.save(input);
        Integer result = userService.findByUseripOrCreate(input.getUserip()).getCount();

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    void countPlus() {

        //given
        UserInfo input = new UserInfo("ip_example", 5);
        Integer expectedResult = 6;

        //run
        userService.countPlus(input.getUserip());
        Integer result = userService.findByUseripOrCreate(input.getUserip()).getCount();

        //assert
        Assert.assertEquals(expectedResult, result);
    }
}