package vk.intern.nedra.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vk.intern.nedra.model.UserInfo;
import vk.intern.nedra.repository.UserRepository;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserInfo findByUseripOrCreate(String ip) {
        UserInfo user = userRepository.findByUserip(ip);
        if (user == null) {
            user = new UserInfo(ip, 0);
            save(user);
        }
        return user;
    }

    public void save(UserInfo user) {
        userRepository.save(user);
    }

    public void countPlus(String ip) {
        UserInfo user = userRepository.findByUserip(ip);
        user.setCount(user.getCount() + 1);
        save(user);
    }

}
