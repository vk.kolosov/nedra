package vk.intern.nedra.model;

import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "userinfo")
public class UserInfo {

    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    //private Integer id;

    @Id
    @Column(name = "userip")
    private String userip;

    @Column(name = "clickcount")
    private Integer count;

    public UserInfo(String userip, Integer count) {
        this.userip = userip;
        this.count = count;
    }

    public String getUserip() {
        return userip;
    }

    public void setUserip(String userip) {
        this.userip = userip;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
