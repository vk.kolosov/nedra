package vk.intern.nedra.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import vk.intern.nedra.model.UserInfo;
import vk.intern.nedra.service.UserService;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    public final UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/home")
    public String youShouldWork(Model model, HttpServletRequest request) {
        UserInfo user = userService.findByUseripOrCreate(request.getRemoteAddr());
        model.addAttribute("userinfo", user);
        return "home";
    }

    @PostMapping("/countplus")
    public String plusClick(HttpServletRequest request) {
        userService.countPlus(request.getRemoteAddr());
        return "redirect:/home";
    }

}
