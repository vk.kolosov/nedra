package vk.intern.nedra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NedraApplication {

    public static void main(String[] args) {
        SpringApplication.run(NedraApplication.class, args);
    }

}
