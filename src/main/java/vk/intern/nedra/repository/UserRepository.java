package vk.intern.nedra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vk.intern.nedra.model.UserInfo;

@Repository
public interface UserRepository extends JpaRepository<UserInfo, Integer> {

    UserInfo findByUserip(String ip);

}
